# Introduction

This is a small nodejs app that fires a get request to the flickr api to search for 10 photos then download them and modifiy their size.

# Install

`npm install`

# Start

`npm run dev`

# Setup

## Required tools:

- Nodejs LTS version >8.0: https://nodejs.org/en/download/
- Flickr Api credentials: https://www.flickr.com/services/api/

## Recommended tools:

- [Visual studio code](https://code.visualstudio.com/download)
- [Postman](https://www.getpostman.com/downloads/)

# Procedure:

- sign in to flickr api using your yahoo mail (You need to have one). Once you log in an api key and secret will be automatically generated.
- Then follow the steps to get the access token and access token secret: https://www.flickr.com/services/api/auth.oauth.html
- After cloning the project and opening it, you will need to create .env file to store the following flickr api credentials:

```bash
FLICKR_CONSUMER_KEY = your api key
FLICKR_CONSUMER_SECRET = your api secret
FLICKR_OAUTH_TOKEN = access token
FLICKR_OAUTH_TOKEN_SECRET = access token secret
FLICKR_USER_ID= your user id
```

# Endpoint

GET/ [http://localhost:3000/api/v1/photos/download]

# Json Example

```json
{ "data": [{ "text": "cat" }, { "tags": "dog" }] }
```

# Overview

- It Accepts a list of search keywords as arguments stored as an array of objects as seen in the JSON.

- Queries the Flickr API for the top-rated image for each keyword:
  We call flickr.photos.search() then pass:
  media: "photos", //to retrieve photos only
  per_page: 1, //to retrieve one image
  sort: "interestingness-desc" //sort them by most interesting
    
   and finally we pass the object entered by the user:
  {
  "tags": "dog"
  }
- Downloads the results
  We call flickr.photos.getSizes() to get the url and the size of the photo. We pass the api_key and photo_id that is previously fetched from flickr.photos.search()
- Writes the result to a user-supplied filename:
  Use the download image module to save the image with a supplied filename and file type.
- Crops them rectangularly
  Using jimp module we can resize the photo then save it again.
- If given less than ten keywords, or if any keyword fails to result in a match, retrieve random words from a dictionary.
  We check the array length if its less than 10 then we use our local dictionary to complete the the keywords. Then we check if a keyword did not have a result
  then get us a word from the dictionary and call flickr.photos.search() passing the dictionary keyword.
  //Each dictionary keyword can be used during this process to in order to avoid duplicated photos.
- Photos will be downloaded in the downloads folder.

- See url [here](https://www.flickr.com/services/api/flickr.photos.search.html) for supported flickr.photos.search()
