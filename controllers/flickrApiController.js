const fs = require('fs');
const Flickr = require("flickr-sdk");
const download = require('image-downloader');
const Jimp = require('jimp');
const flickr = new Flickr(
    Flickr.OAuth.createPlugin(
        process.env.FLICKR_CONSUMER_KEY,
        process.env.FLICKR_CONSUMER_SECRET,
        process.env.FLICKR_OAUTH_TOKEN,
        process.env.FLICKR_OAUTH_TOKEN_SECRET
    )
);
module.exports = app => {
    //get photo size, URL and download image
    app.get("/api/v1/photos/download", async (req, res) => {
        try {
            let searchRequest = req.body.data; //get body requested data
            let contents =  fs.readFileSync("dictionary/words.txt").toString('utf-8'); //read dicionary words from local file
            let words =  JSON.parse(contents) //convert it to json data to match our input //words is an array of objects
            if (searchRequest.length < 10) {
                for (let i = searchRequest.length; i < 10; i++) {
                    //get a random item from an array (in our case to complete the image search)
                    //Each dictionary keyword can be used during this process to in order to avoid duplicated photos.
                    let randIndex = parseInt(Math.floor(Math.random() * words.data.length));
                    let randomItem = words.data[randIndex];
                    words.data.splice(randIndex, 1); //delete the fetched keyword
                    searchRequest.push(randomItem);  //push item to array
                }
            }
            res.status(200).send({
                message:"Photos downloaded successfully"
            });
            await Promise.all(searchRequest.map(async query => {
                //search flickr api for photos using the provided search criteria
                let retrievedPhotos = await flickr.photos.search({
                    ...query,
                    media: "photos",
                    per_page: 1, //to retrieve one image
                    sort: "interestingness-desc" //sort them by most interesting
                });
                //in case search fails then retrieve data from our local dictionary
                if (retrievedPhotos.body.photos.total == "0") {
                    let randIndex = parseInt(Math.floor(Math.random() * words.data.length));
                    let randomItem = words.data[randIndex];
                    words.data.splice(randIndex, 1);
                    retrievedPhotos = await flickr.photos.search({
                        ...randomItem,
                        media: "photos",
                        per_page: 1,
                        sort: "interestingness-desc"
                    });
                }
                //according to Flickr Api in order to access the data we have to call .body to the result of the call
                await Promise.all(retrievedPhotos.body.photos.photo.map(async attribute => {
                    //get photo sizes and URL
                    const retrievedSizesURL = await flickr.photos.getSizes({
                        api_key: process.env.FLICKR_CONSUMER_KEY,
                        photo_id: attribute.id
                    });
                    //image name fix if it containts / then it treats it as path so we have to remove it from img name.
                    let title = attribute.title.replace(/[^\w\s]/gi, ' '); //alow only digits, letters, underscore and whitespace
                    const imagePath = `downloads/${title}.png`;
                    const options = {
                        url: retrievedSizesURL.body.sizes.size[6].source, //get the medium size url from size array
                        dest: imagePath
                    }
                    await download.image(options); //download image
                    await Jimp.read(imagePath, (err, resultPhoto) => {
                        if (err) throw err;
                        resultPhoto
                            .resize(480, 268) // resize
                            .write(imagePath); // save
                    });
                }));
            }));
        } catch (err) {
            console.log(err);
            res.status(400).send(err);
        }
    });
};