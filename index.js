const dotenv = require("dotenv");
dotenv.config();
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

var corsOptions = {
  origin: "*",
  methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
  preflightContinue: true,
  optionsSuccessStatus: 200
};

app.options("*", cors(corsOptions));
app.use(cors(corsOptions));
app.use(bodyParser.json({ type: "*/json" }));

require("./controllers/flickrApiController")(app);

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Server is running on Port ${port}`));